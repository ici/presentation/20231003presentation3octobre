\documentclass[xcolor=table]{beamer}
\usetheme[progressbar=frametitle, sectionpage=none]{metropolis}
\usepackage{appendixnumberbeamer}
\usepackage{amssymb, amsmath}
\usepackage[absolute,overlay]{textpos}
\usepackage{booktabs}
\usepackage{pgf, tikz}
\usepackage{pdfcomment}
\usepackage{pgfplots}
\usepackage{algpseudocode}
\usepackage{algorithm}
\usepackage{hyperref}
\usepackage[utf8]{inputenc}
\usepackage{xspace}
\usepackage{color}
\usepackage{animate}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}
\newcommand\FontImportant{\fontsize{15}{20}\selectfont}
\newcommand\FontMoyenImportant{\fontsize{14}{17}\selectfont}
\newcommand\FontPetit{\fontsize{8}{6}\selectfont}
\definecolor{links}{HTML}{2A1B81}
\hypersetup{colorlinks,linkcolor=,urlcolor=links}
\definecolor{green}{cmyk}{1,0,1,0.12}

\title{Point du 3 octobre 2023}
\author{Maxime Colomb}

\titlegraphic{\vspace{-0.cm}\includegraphics[height=1.1cm]{img/logoIGN.png}\hfill\includegraphics[height=1.1cm]{img/inr_logo_noir.png}}
%\date{10 Novembre 2022}

\makeatletter
\newcommand\addsectiontotoc[1]{%
  \addtocontents{toc}{%
    \protect\beamer@sectionintoc{\the\c@section}{#1}{\the\c@page}{\the\c@part}%
                                {\the\beamer@tocsectionnumber}}
}
\makeatother
\newcommand{\MYhref}[3][blue]{\href{#2}{\color{#1}{#3}}}%\right) 
\makeatletter
\newcommand{\miniscule}{\@setfontsize\miniscule{4}{5}}% \tiny: 5/6
\makeatother

\setbeamerfont*{section in head/foot}{size=\miniscule}
\setbeamercolor{section in head/foot}{parent=palette primary}
\setbeamerfont{title}{size=\large}
\setbeamerfont{date}{size=\scriptsize}

\addtobeamertemplate{frametitle}{}{%
  \begin{textblock*}{5cm}(11.3cm,0.03cm)
    \insertsectionnavigation{4cm}
  \end{textblock*}
}


\defbeamertemplate{section page}{progressbarcomplete}{
  \centering
  \begin{minipage}{22em}
    \raggedright
    \usebeamercolor[fg]{section title}
    \usebeamerfont{section title}
    \insertsection\\[-1ex]
    \usebeamertemplate*{progress bar in section page}
    \par
    \ifx\insertsubsection\@empty\else%
    \usebeamercolor[fg]{subsection title}%
    \usebeamerfont{subsection title}%
    \insertsubsection
    \fi
  \end{minipage}
  \par
  \vspace{\baselineskip}
}
\begin{document}

\maketitle



\begin{frame}{Sommaire :}
	\begin{itemize}
		\item Évolutions du simulateur
		\item Distribution des calculs
		\begin{itemize}
			\small
			\item Explorations du modèle avec OpenMole
			\item Calcul massif sur IRENE-ROME
		\end{itemize}
		\item Site web
		\item Évolutions du générateur de données
		\item Discussions 
	\end{itemize}
\end{frame}


\section{Évolutions du simulateur}
\begin{frame}{}
	\sectionpage
\end{frame}

\begin{frame}{Correction de bugs et améliorations}
	\only<1>{\begin{itemize}
		\item Correction de la population observée dans les indicateurs :
		\begin{description}
			\small
			\item Confusions entre la population observée qui habite dans la zone et qui passe par la zone. 
			\item \textbf{Le taux d'incidence était bien trop élevé !}
		\end{description}
		\item Performances améliorées (réécritures de fonctions, mise en cache de valeurs beaucoup utilisées)
		\item Ajout de bonnes pratiques et code plus clair…
	\end{itemize}}
	\only<2->{
		\only<2>{
			\includegraphics[width=0.6\textwidth]{img/v0.0.4.png}
			\vfill
			\textit{Avec la version 0.0.4, présentée lors de la réunion au CRESS. 100 jours sur le 11ème arrondissement}
		
	}	
		\only<3>{\includegraphics[width=0.6\textwidth]{img/v0.0.5-SNAPSHOT.png}
		\vfill
		\textit{Avec la version 0.0.5-SNAPSHOT actuelle. 100 jours sur le 11ème arrondissement}
		
	}
}
	
	
\end{frame}

\section{Distribution des calculs}
\begin{frame}{}
	\sectionpage
\end{frame}

\begin{frame}{Fonctionnement du code sur OpenMOLE et le cluster Margaret}
	\begin{block}{OpenMOLE :}
			\centering \includegraphics[width=0.3\textwidth]{img/om.png}
		\\
		\textit{Model as a \textbf{black box}} avec les entrée et sorties prédéfinies. 
		\\
		Distributions sur différentes infrastructures de calcul. 
	\end{block}
\begin{block}{Exploration des modèles :}
		\begin{itemize}
		\item Applications d'algorithmes de la littérature
		\item Gestion intégrée de la stochasticité 
	\end{itemize}
	
\end{block}
	\vfill
	\begin{block}{Deux explorations préliminaires :}
		\begin{enumerate}
			\item \textbf{Optimisation multi-critères} sur les critères de fermeture des classes. 
			\item Analyse de \textbf{sensibilité globale} sur les paramètres de l'équation de contamination.
		\end{enumerate}
	\end{block}
	
	
\end{frame}

\begin{frame}{Explorations avec OpenMOLE : Optimisation multi-critères}
	\begin{block}{Paramètres de variation}
		\begin{itemize}
			\item Taux d'incidence à partir duquel les classes sont fermées (20 à 1000).
			\item Nombre de jours de fermeture de classes (2 à 25).
			\item Seed aléatoire (\href{https://next.openmole.org/Stochasticity+Management.html}{gestion de la stochasticité intégrée}).
		\end{itemize}
	\end{block} 
	\begin{block}{Critères d'optimisation}
		\begin{itemize}
			\item Nombre total de jours de classes fermées.
			\item Nombre total de contaminations.
		\end{itemize}
	\end{block} 
	\vfill
	\begin{block}{Détails techniques}
		\vfill
		\href{https://next.openmole.org/Calibration.html}{Algorithme NSGA2}
		\\
		300 tests
	\end{block}
\end{frame}
\begin{frame}{Optimisation multi-critères - Résultats}
	\only<1>{\includegraphics[width=\textwidth]{img/contXclass.png}}
	\only<2>{\includegraphics[width=\textwidth]{img/thresholdXtime.png}}
	\only<3>{\includegraphics[width=\textwidth]{img/thresXclass.png}}
	\only<4>{\includegraphics[width=\textwidth]{img/thresXcont.png}}
	\only<5>{\includegraphics[width=\textwidth]{img/timeXclass.png}}
	\only<6>{\includegraphics[width=\textwidth]{img/timeXcont.png}}
		
\end{frame}
\begin{frame}{Explorations avec OpenMOLE : Étude de sensibilité globale}
	\begin{block}{}
		\footnotesize
		\textit{Tirée de (Saltelli 2010) et \href{https://next.openmole.org/Sensitivity.html\#Saltellismethod}{implémenté dans OpenMOLE}} 
	\end{block}
	\vfill
	\begin{block}{Description}
		\vfill
		Estimation d'indices de sensibilité concernant la variance relative quant à la variation d'autre paramètres sur un indicateur défini.
		\vfill
		\textit{Indicateur choisi} : \textbf{Somme de personne contaminés}.
	\end{block}
	\includegraphics[width=0.5\textwidth]{img/sensi.png}
\end{frame}
\begin{frame}{Étude de sensibilité globale - Résultats}
\only<1>{
	\begin{block}{Estimation des indices de premier ordre}
		\begin{quote}
			Pour un facteur $x_{i}$ et un indicateur $y_{j}$, l'indice de premier ordre est calculé pour chaque $x_{i}$ en estimant l'espérence de $y_{j}$ conditionnellement aux valeurs de $x_{i}$ avec une variation de tous les autres facteurs. 
		\end{quote}
	\vfill
		$S_{i} =  \frac{V_{X_{i}}(E_{X_{\sim i}}(y_{j} | x_{i}))}{Var[y_{j}]} $
		\\
		\textit{where $X_{\sim i}$ are all other factors but $x_{i}$}
	\end{block}
	\vfill
	\includegraphics[width=\textwidth]{img/firstOrder.png}
}	\only<2>{	 \begin{block}{Estimation  des indices d'ordre total}
		\begin{quote}
			Pour un facteur $x_{i}$ et un indicateur $y_{j}$, l'indice d'ordre total est calculé pour chaque $x_{i}$ en calculant le comportement de $y_{j}$ conditionnellement aux valeurs de $x_{i}$ avec une variation de tous les autres facteurs. 
		\end{quote}
	\vfill
			$S_{i} =  \frac{E_{X_{\sim i}}(V_{x_{i}}(y_{j} | X_{\sim i}))}{Var[y_{j}]} $
	\\
	\textit{where $X_{\sim i}$ are all other factors but $x_{i}$}
\end{block}
\vfill
	\includegraphics[width=\textwidth]{img/totalOrder.png}
}
\end{frame}


\begin{frame}{Distribution sur IRENE-ROME}
	La personne de l'INRIA me réponds par email une fois toute les deux semaines…
	\begin{block}{}
		Calcul prêt. Adaptation du script SLURM déjà faite par Nicolas pour cluster Moab. 
	\end{block}
\begin{block}
	\vfill
	$ \triangleright $ Pour tous les arrondissements,
	\\
	\setlength\parindent{14pt}$ \triangleright $ Pour quelques combinaisons de paramètres de l'équation de contamination,
	\\
	\setlength\parindent{30pt}$ \triangleright $ Pour différentes interventions sanitaires (confinement, 2 scénarios de fermeture des écoles avec seuil, 2 scénarios de fermeture individualisées des écoles, valeurs choisies grâce aux optimisations multi-critères).
\end{block}
\end{frame}
\section{Site web}
\begin{frame}{}
	\sectionpage
\end{frame}

\begin{frame}{Entrevue avec le SED}
	Réunion Romain et Sébastien le 25 septembre.
	\vfill
	Recommandations : 
	\begin{itemize}
		\footnotesize
		\item Faire des tests avec un public cible d'épidémio 
		\item En fonction, refondre l'\textit{user experience} pour que çe soit intuitif d'utilisation.
		\item Se rapprocher de l'équipe de l'INRIA xxx comme conseillé lors du CEP.
		\item Mettre en place des bonnes pratiques de programmation pour que çe soit facile de s'insérer dans le projet,
	\end{itemize}
	 \vfill
	 Éventuellement, une session de travail pourrait être envisageable pour me débloquer de mon problème technique dans un temps non-défini.
	 \vfill
	 J'ai donc retroussé mes manches.
\end{frame}

\begin{frame}{Réécriture du back-end en PHP}
	Plus simple avec un vrai langage informatique !
	\begin{block}{Ajouts}
		\begin{itemize}
			\footnotesize
			\item Combinatoire imparfaite 
			\item Version du simulateur
		\end{itemize}
	\end{block}
	\vfill
	\begin{block}{Reste à faire}
		\begin{itemize}
			\footnotesize
			\item Encore un peu de plomberie (statistiques sur la population, sous-populations à afficher (pas implémenté par Nicolas))
			\item Multiples petits bugs
			\item Sélection des interventions sanitaires. Faire un catalogue ?
		\end{itemize}
	\end{block}
	Toujours \url{https://ici.saclay.inria.fr/dist/}
	\vfill
	\textit{\textbf{Je suis intéressé par vos retours}}
\end{frame}




\section{Évolutions du générateur de données}
\begin{frame}{}
	\sectionpage
\end{frame}

\begin{frame}{Avancements}
	Division du code en cinq projets distincts. 
	\uncover<2>{(Travail sur la suppression des inter-liens)}:
	\vfill
	{\centering
	\only<1>{\includegraphics[height=0.6\textheight]{img/projetICI.png}}
	\only<2>{\includegraphics[height=0.6\textheight]{img/projetICImieux.png}}}
\end{frame}

\begin{frame}{Généralisation des calculs : Application sur une nouvelle ville }
	\textit{Quelle est cette ville ? }
	\vfill
	\includegraphics[height=0.8\textheight]{img/lyon.png}
	\\
	
	\uncover<2>{Lyon !}
\end{frame}

\begin{frame}{Écriture de l'article en cours}
	Journal ciblé : International Journal of Geographical Information Science.
	\vfill
	Une partie sur trois rédigée. 	
	\vfill
	Planification d'une journée avec Julien et d'autre chercheurs de l'IGN.
\end{frame}

\section{Discussions diverses}
\begin{frame}{}
\sectionpage
\end{frame}


\begin{frame}{Conférence \textit{the scale of epidemic modelling}}
	Multi-échelle non pas entre les échelles géographiques mais entre les simulations intra-individus et de contaminations individu-individu. 
	\vfill
	Des questions sur l'utilisation de données mobiles. 
	Intégrer ces travaux dans le module "présence" ?
	Retravail sur ce module à l'occasion du PEPR \textit{Data Technology for MOBILITY in the Territories} ?
\end{frame}

\begin{frame}{Qu'en est il de nos échelles de simulation ?}
	Pour l'instant, un arrondissement avec des flux 
	\vfill
	Possibilité de simuler plusieurs arrondissements en même temps mais les flux sont comptés en double.
	\vfill
	Séance dédiée pour répondre à cette question ? Une demi journée lors du séminaire ASCII pourrait être consacrée à cette question. 
\end{frame}
\begin{frame}{Autre points ? }
	\begin{block}{Présentation au groupe de travail d'épidémiologistes \textit{"Echelles mésoscopiques"} fin novembre/début décembre.}
		\vfill
		Denis prépare une intervention sur la vue d'ensemble/le positionnement/les attentes et objectifs du projet.
		\vfill
		Maxime prépare son intervention sur l'aspect plus technique. 
	\end{block}
	\vfill 
	Des nouvelles du CRESS ? 
\end{frame}
\end{document}